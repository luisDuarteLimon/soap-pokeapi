//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.04.11 at 11:32:45 PM CDT 
//


package com.challenge.soap_pokemon;

import com.challenge.soap.pokemon.model.Ability;
import com.challenge.soap.pokemon.model.Item;
import com.challenge.soap.pokemon.model.LocationAreaEncounter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;


/**
 * <p>Java class for pokemonResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="pokemonResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="abilities" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="baseExperience" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="locationAreaEncounters" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pokemonResponse", propOrder = {
    "name",
    "abilities",
    "items",
    "baseExperience",
    "id",
    "locationAreaEncounters"
})
public class PokemonResponse {

    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    protected List<Ability> abilities;
    @XmlElement(required = true)
    protected List<Item> items;
    @XmlElement(required = true)
    protected String baseExperience;
    @XmlElement(required = true)
    protected int id;
    @XmlElement(required = true)
    protected List<LocationAreaEncounter> locationAreaEncounters;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the abilities property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public List<Ability> getAbilities() {
        return abilities;
    }

    /**
     * Sets the value of the abilities property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAbilities(List<Ability> value) {
        this.abilities = value;
    }

    /**
     * Gets the value of the abilities property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * Sets the value of the abilities property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setItems(List<Item> value) {
        this.items = value;
    }


    /**
     * Gets the value of the baseExperience property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseExperience() {
        return baseExperience;
    }

    /**
     * Sets the value of the baseExperience property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseExperience(String value) {
        this.baseExperience = value;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the locationAreaEncounters property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public List<LocationAreaEncounter> getLocationAreaEncounters() {
        return locationAreaEncounters;
    }

    /**
     * Sets the value of the locationAreaEncounters property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationAreaEncounters(List<LocationAreaEncounter> value) {
        this.locationAreaEncounters = value;
    }

}
