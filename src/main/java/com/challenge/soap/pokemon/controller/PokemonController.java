package com.challenge.soap.pokemon.controller;


import com.challenge.soap.pokemon.service.IPokemonService;
import com.challenge.soap.pokemon.service.PokemonService;
import com.challenge.soap_pokemon.GetPokemonRequest;
import com.challenge.soap_pokemon.GetPokemonResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.servlet.http.HttpServletRequest;
/**
 * @author Jose Luis Duarte L.
 * @Description The file {@link PokemonService}
 * @project soap-pokemon
 * @created 12/04/21
 */
@Endpoint
@RequiredArgsConstructor
public class PokemonController {
    /**
     *  class service contains the logic
     */
    private final IPokemonService pokemonService;

    /**
     * contains client ip
     */
    private HttpServletRequest httpServletRequest;

    @Autowired
    public void setRequest(HttpServletRequest request) {
        this.httpServletRequest = request;
    }

    /**
     *
     * @param getPokemonRequest class with request information
     * @return Object with the response according action
     */

    @PayloadRoot(namespace = "http://challenge.com/soap-pokemon",
            localPart = "getPokemonRequest")
    @ResponsePayload
    public GetPokemonResponse getInformationPokemon(@RequestPayload GetPokemonRequest getPokemonRequest) {


        return pokemonService.getPokemonService(getPokemonRequest, httpServletRequest);

    }


}
