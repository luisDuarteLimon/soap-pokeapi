package com.challenge.soap.pokemon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoapPokemonApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoapPokemonApplication.class, args);
	}

}
