package com.challenge.soap.pokemon.transformers;

import com.challenge.soap.pokemon.model.GetInformationRequest;
import com.challenge.soap.pokemon.service.PokemonService;
import com.challenge.soap_pokemon.GetPokemonRequest;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
/**
 * @author Jose Luis Duarte L.
 * @Description The file {@link PokemonService}
 * @project soap-pokemon
 * @created 12/04/21
 */
@Component
public class InfoRequestTransformer {


    /**
     *
     * @param pokemonRequest object with request
     * @param httpServletReques object with client ip
     * @return Object with information for database
     */
    public GetInformationRequest mapToGetInformationRequest(GetPokemonRequest pokemonRequest, HttpServletRequest httpServletReques) {

        GetInformationRequest getInformationRequest = new GetInformationRequest();
        getInformationRequest.setDateRequest(LocalDateTime.now());
        getInformationRequest.setIpOrigin(httpServletReques.getRemoteAddr());
        getInformationRequest.setMethod(pokemonRequest.getAction());

        return getInformationRequest;


    }


}
