package com.challenge.soap.pokemon.transformers;


import com.challenge.soap.pokemon.model.Ability;
import com.challenge.soap.pokemon.model.Item;
import com.challenge.soap.pokemon.model.LocationAreaEncounter;
import com.challenge.soap.pokemon.model.Pokemon;
import com.challenge.soap.pokemon.service.PokemonService;
import com.challenge.soap_pokemon.GetPokemonResponse;
import com.challenge.soap_pokemon.PokemonResponse;
import org.springframework.stereotype.Component;

import java.util.List;
/**
 * @author Jose Luis Duarte L.
 * @Description The file {@link PokemonService}
 * @project soap-pokemon
 * @created 12/04/21
 */
@Component
public class PokemonTransformer {

    public GetPokemonResponse maToPokemonAbilityResponse(String namePokemon, List<Ability> abilities) {
        GetPokemonResponse getPokemonResponse = new GetPokemonResponse();
        PokemonResponse pokemonResponse = new PokemonResponse();
        pokemonResponse.setAbilities(abilities);
        pokemonResponse.setName(namePokemon);
        getPokemonResponse.setPokemonResponse(pokemonResponse);
        return getPokemonResponse;
    }

    public GetPokemonResponse maToPokemonExperienceResponse(Pokemon responseAPI) {
        GetPokemonResponse getPokemonResponse = new GetPokemonResponse();
        PokemonResponse pokemonResponse = new PokemonResponse();
        pokemonResponse.setBaseExperience(responseAPI.getName()
                .concat(" has ").concat(String.valueOf(responseAPI.getBaseExperience())).concat(" of experience"));
        pokemonResponse.setId(responseAPI.getId());
        getPokemonResponse.setPokemonResponse(pokemonResponse);
        return getPokemonResponse;
    }

    public GetPokemonResponse maToPokemonItemsResponse(String namePokemon, List<Item> itemResponse) {
        GetPokemonResponse getPokemonResponse = new GetPokemonResponse();
        PokemonResponse pokemonResponse = new PokemonResponse();
        pokemonResponse.setItems(itemResponse);
        pokemonResponse.setName(namePokemon);
        getPokemonResponse.setPokemonResponse(pokemonResponse);
        return getPokemonResponse;
    }


    public GetPokemonResponse maToPokemonIdResponse(Pokemon responseAPI) {
        GetPokemonResponse getPokemonResponse = new GetPokemonResponse();
        PokemonResponse pokemonResponse = new PokemonResponse();
        pokemonResponse.setId(responseAPI.getId());
        getPokemonResponse.setPokemonResponse(pokemonResponse);
        return getPokemonResponse;
    }


    public GetPokemonResponse maToPokemonNameResponse(Pokemon pokemon) {
        GetPokemonResponse getPokemonResponse = new GetPokemonResponse();
        PokemonResponse pokemonResponse = new PokemonResponse();
        pokemonResponse.setName(pokemon.getName());

        getPokemonResponse.setPokemonResponse(pokemonResponse);
        return getPokemonResponse;
    }

    public GetPokemonResponse maToPokemonLocalAreaResponse(List<LocationAreaEncounter> locationAreaEncounter) {
        GetPokemonResponse getPokemonResponse = new GetPokemonResponse();
        PokemonResponse pokemonResponse = new PokemonResponse();
        pokemonResponse.setLocationAreaEncounters(locationAreaEncounter);

        getPokemonResponse.setPokemonResponse(pokemonResponse);
        return getPokemonResponse;
    }
}
