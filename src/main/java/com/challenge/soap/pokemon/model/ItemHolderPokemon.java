package com.challenge.soap.pokemon.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemHolderPokemon {

    @JsonProperty("pokemon")
    private NamedApiResource pokemon;

    @JsonProperty("version_details")
    private List<ItemHolderPokemon> versionDetails;
}
