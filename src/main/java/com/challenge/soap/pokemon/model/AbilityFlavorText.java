package com.challenge.soap.pokemon.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class AbilityFlavorText {

    @JsonProperty("flavor_text")
    private String flavorText;

    @JsonProperty("language")
    private NamedApiResource language;

    @JsonProperty("version_group")
    private NamedApiResource versionGroup;


}
