package com.challenge.soap.pokemon.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Effect {

    @JsonProperty("effect")
    private String effect;

    @JsonProperty("language")
    private NamedApiResource language;
}
