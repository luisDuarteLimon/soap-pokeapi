package com.challenge.soap.pokemon.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("cost")
    private Integer cost;

    @JsonProperty("fling_power")
    private Integer flingPower;

    @JsonProperty("fling_effect")
    private NamedApiResource flingEffect;

    @JsonProperty("category")
    private NamedApiResource category;

    @JsonProperty("effect_entries")
    private List<VerboseEffect> effectEntries;

    @JsonProperty("flavor_text_entries")
    private List<VersionGroupFlavorText> flavortextTntries;

    @JsonProperty("game_indices")
    private List<GenerationGameIndex> gameIndices;

    @JsonProperty("names")
    private List<Name> names;

    @JsonProperty("sprites")
    private ItemSprites sprites;

    @JsonProperty("held_by_pokemon")
    private List<ItemHolderPokemon> heldByPokemon;

    @JsonProperty("machines")
    private List<MachineVersionDetail> machines;






}
