package com.challenge.soap.pokemon.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class VersionGroupFlavorText {

    @JsonProperty("text")
    private String text;

    @JsonProperty("language")
    private NamedApiResource language;

    @JsonProperty("version_group")
    private NamedApiResource versionGroup;

}
