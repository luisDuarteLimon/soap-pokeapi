package com.challenge.soap.pokemon.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class PokemonAbility {

    @JsonProperty("is_hidden")
    private Boolean isHidden;

    @JsonProperty("slot")
    private int slot;

    @JsonProperty("ability")
    private NamedApiResource ability;


}