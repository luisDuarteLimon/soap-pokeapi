package com.challenge.soap.pokemon.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pokemon {

    @JsonProperty("id")
    private int id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("location_area_encounters")
    private String locationAreaEncounters;

    @JsonProperty("base_experience")
    private int baseExperience;

    @JsonProperty("height")
    private int height;

    @JsonProperty("is_default")
    private Boolean isDefault;

    @JsonProperty("order")
    private int order;

    @JsonProperty("species")
    private NamedApiResource species;

    @JsonProperty("abilities")
    private List<PokemonAbility> abilities;

    @JsonProperty("forms")
    private List<NamedApiResource> forms;

    @JsonProperty("held_items")
    private List<PokemonHeldItem> heldItems;

    @JsonProperty("moves")
    private List<PokemonMove> moves;

    @JsonProperty("stats")
    private List<PokemonStat> stats;

    @JsonProperty("types")
    private List<PokemonType> types;



}
