package com.challenge.soap.pokemon.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class PokemonMoveVersion {

    @JsonProperty("move_learn_method")
    private NamedApiResource moveLearnMethod;

    @JsonProperty("version_group")
    private NamedApiResource versionGroup;

    @JsonProperty("level_learned_at")
    private int levelLearnedAt;
}
