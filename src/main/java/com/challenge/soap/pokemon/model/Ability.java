package com.challenge.soap.pokemon.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ability {

    @JsonProperty("id")
    private int id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("is_main_series")
    private Boolean isMainSeries;

    @JsonProperty("generation")
    private NamedApiResource generation;

    @JsonProperty("names")
    private List<Name> names;

    @JsonProperty("effect_entries")
    private  List<VerboseEffect> effectEntries;

    @JsonProperty("effect_changes")
    private List<AbilityEffectChange>  effectChanges;

    @JsonProperty("flavor_text_entries")
    private List<AbilityFlavorText> flavorTextEntries;

    @JsonProperty("pokemon")
    private List<AbilityPokemon> pokemon;



}
