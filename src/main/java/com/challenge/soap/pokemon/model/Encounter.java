package com.challenge.soap.pokemon.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Encounter {

    @JsonProperty("min_level")
    private Integer minLevel;

    @JsonProperty("max_level")
    private Integer maxLevel;

    @JsonProperty("chance")
    private Integer chance;

    @JsonProperty("method")
    private NamedApiResource method;

}
