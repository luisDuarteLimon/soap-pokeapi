package com.challenge.soap.pokemon.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class VersionEncounterDetail {

    @JsonProperty("version")
    private  NamedApiResource version;

    @JsonProperty("max_chance")
    private Integer maxChance;

    @JsonProperty("encounter_details")
    private List<Encounter> encounterDetails;
}
