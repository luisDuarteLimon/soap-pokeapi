package com.challenge.soap.pokemon.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class VerboseEffect {

    @JsonProperty("effect")
    private String effect;

    @JsonProperty("short_effect")
    private String shortEffect;

    @JsonProperty("language")
    private NamedApiResource language;

}
