package com.challenge.soap.pokemon.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class SoapWebServiceConfig extends WsConfigurerAdapter {


    @Bean
    public ServletRegistrationBean messageDispatcherServelet(ApplicationContext context){

        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(context);
        servlet.setTransformWsdlLocations(true);

        return new ServletRegistrationBean(servlet,"/pokeapi/*");
    }

    @Bean
    public XsdSchema pokemonSchema(){
       return new SimpleXsdSchema(new ClassPathResource("namePokemon.xsd"));
    }

    @Bean
    public DefaultWsdl11Definition deaDefaultWsdl11Definition(XsdSchema pokemonSchema){
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setSchema(pokemonSchema);
        definition.setLocationUri("/pokeapi");
        definition.setPortTypeName("PokemonService");
        definition.setTargetNamespace("http://challenge.com/soap-pokemon");
        return definition;

    }

}
