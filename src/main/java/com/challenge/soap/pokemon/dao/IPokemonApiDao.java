package com.challenge.soap.pokemon.dao;

import com.challenge.soap.pokemon.model.Ability;
import com.challenge.soap.pokemon.model.Item;
import com.challenge.soap.pokemon.model.LocationAreaEncounter;
import com.challenge.soap.pokemon.model.Pokemon;

import java.util.List;

/**
 * @author Jose Luis Duarte L.
 * @Description The file {@link IPokemonApiDao}
 * @project soap-pokemon
 * @created 12/04/21
 */
public interface IPokemonApiDao {

    /**
     * this method gets pokemon information
     *
     * @param namePokemon pokemon name
     * @return object with pokemon information
     */
    Pokemon getPokemon(String namePokemon);

    /**
     *this method gets abilities information
     *
     * @param url  get Abilities
     * @return object with abilities information
     */
    Ability getAbility(String url);

    /**
     *this method gets item information
     *
     * @param url  get items
     * @return object with item information
     */
    Item getItem(String url);

    /**
     *this method gets location information
     *
     * @param url  get pokemon locations
     * @return object with location information
     */
    List<LocationAreaEncounter> getLocalAreaEncounter(String url);
}
