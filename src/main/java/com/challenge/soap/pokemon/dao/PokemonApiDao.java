package com.challenge.soap.pokemon.dao;


import com.challenge.soap.pokemon.model.Ability;
import com.challenge.soap.pokemon.model.Item;
import com.challenge.soap.pokemon.model.LocationAreaEncounter;
import com.challenge.soap.pokemon.model.Pokemon;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author Jose Luis Duarte L.
 * @Description The file {@link PokemonApiDao}
 * @project soap-pokemon
 * @created 12/04/21
 */
@RequiredArgsConstructor
@Service
public class PokemonApiDao {

    /**
     * object for request from pokeApi
     */
    private final RestTemplate restTemplate;

    /**
     * this method gets pokemon information
     *
     * @param namePokemon pokemon name
     * @return object with pokemon information
     */
    public Pokemon getPokemon(String namePokemon) {

        String url = "https://pokeapi.co/api/v2/pokemon/".concat(namePokemon);

        ResponseEntity<Pokemon> response = restTemplate.getForEntity(url, Pokemon.class);

        return response.getBody();


    }

    /**
     *this method gets abilities information
     *
     * @param url  get Abilities
     * @return object with abilities information
     */
    public Ability getAbility(String url) {


        ResponseEntity<Ability> response = restTemplate.getForEntity(url, Ability.class);


        return response.getBody();


    }


    /**
     *this method gets item information
     *
     * @param url  get items
     * @return object with item information
     */
    public Item getItem(String url) {

        ResponseEntity<Item> response = restTemplate.getForEntity(url, Item.class);

        return response.getBody();


    }

    /**
     *this method gets location information
     *
     * @param url  get pokemon locations
     * @return object with location information
     */
    public List<LocationAreaEncounter> getLocalAreaEncounter(String url){

        ResponseEntity<List<LocationAreaEncounter>> response = restTemplate.
                exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<LocationAreaEncounter>>() {
                });

        return response.getBody();


    }

}
