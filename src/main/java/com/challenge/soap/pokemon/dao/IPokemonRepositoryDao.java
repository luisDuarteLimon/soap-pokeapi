package com.challenge.soap.pokemon.dao;

import com.challenge.soap.pokemon.model.GetInformationRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPokemonRepositoryDao extends CrudRepository<GetInformationRequest,Integer> {


}
