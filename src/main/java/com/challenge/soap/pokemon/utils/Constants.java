package com.challenge.soap.pokemon.utils;

public final class Constants {

    public static final String ABILITIES ="abilities";

    public static final String EXPERIENCIE ="experience";

    public static final String ITEMS ="items";

    public static final String ID="id";

    public static final String NAME="name";

    public static final String LOCATION="location";

    private Constants() {

    }
}
