package com.challenge.soap.pokemon.service;


import com.challenge.soap.pokemon.dao.IPokemonRepositoryDao;
import com.challenge.soap.pokemon.dao.PokemonApiDao;
import com.challenge.soap.pokemon.model.Ability;
import com.challenge.soap.pokemon.model.Item;
import com.challenge.soap.pokemon.model.NamedApiResource;
import com.challenge.soap.pokemon.model.PokemonAbility;
import com.challenge.soap.pokemon.model.PokemonHeldItem;
import com.challenge.soap.pokemon.transformers.InfoRequestTransformer;
import com.challenge.soap.pokemon.transformers.PokemonTransformer;
import com.challenge.soap_pokemon.GetPokemonRequest;
import com.challenge.soap_pokemon.GetPokemonResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.challenge.soap.pokemon.utils.Constants.ABILITIES;
import static com.challenge.soap.pokemon.utils.Constants.EXPERIENCIE;
import static com.challenge.soap.pokemon.utils.Constants.ID;
import static com.challenge.soap.pokemon.utils.Constants.ITEMS;
import static com.challenge.soap.pokemon.utils.Constants.LOCATION;
import static com.challenge.soap.pokemon.utils.Constants.NAME;

/**
 * @author Jose Luis Duarte L.
 * @Description The file {@link PokemonService}
 * @project soap-pokemon
 * @created 12/04/21
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class PokemonService implements IPokemonService {

    /**
     * instance for request pokeApi
     */
    private final PokemonApiDao pokemonApiDao;

    /**
     * instance for data mapper
     */
    private final PokemonTransformer pokemonTransformer;

    /**
     * instance for save dataBase
     */
    private final IPokemonRepositoryDao pokemonRepositoryDao;

    /**
     * instance for data mapper
     */
    private final InfoRequestTransformer infoRequestTransformer;

    /**
     * @param pokemonRequest     object request
     * @param httpServletRequest object with client ip
     * @return object with response according action
     */
    @Override
    public GetPokemonResponse getPokemonService(GetPokemonRequest pokemonRequest, HttpServletRequest httpServletRequest) {

        saveInfoRequest(pokemonRequest, httpServletRequest);

        return evaluateAction(pokemonRequest);
    }

    /**
     * this method gets pokemon abilities
     *
     * @param namePokemon pokemon name
     * @return object with response according action
     */
    private GetPokemonResponse getAbilities(String namePokemon) {

        return Optional.of(pokemonApiDao.getPokemon(namePokemon).getAbilities())
                .map(list -> {
                    List<Ability> abilityList = new ArrayList<>();
                    list.parallelStream()
                            .map(PokemonAbility::getAbility)
                            .map(NamedApiResource::getUrl)
                            .forEach(url -> abilityList.add(pokemonApiDao.getAbility(url)));
                    return abilityList;

                }).map(list -> pokemonTransformer.maToPokemonAbilityResponse(namePokemon, list)).orElse(null);
    }

    /**
     * this method gets pokemon experience
     *
     * @param namePokemon pokemon name
     * @return object with response according action
     */
    private GetPokemonResponse getBaseExperience(String namePokemon) {
        log.info("experience");

        return pokemonTransformer.maToPokemonExperienceResponse(pokemonApiDao.getPokemon(namePokemon));
    }

    /**
     * this method gets pokemon items
     *
     * @param namePokemon pokemon name
     * @return object with response according action
     */
    private GetPokemonResponse getHeldItems(String namePokemon) {

        return Optional.of(pokemonApiDao.getPokemon(namePokemon).getHeldItems())
                .map(list -> {
                    List<Item> itemList = new ArrayList<>();
                    list.parallelStream()
                            .map(PokemonHeldItem::getItem)
                            .forEach(items -> itemList.add(pokemonApiDao.getItem(items.getUrl())));
                    return itemList;
                }).map(list -> pokemonTransformer.maToPokemonItemsResponse(namePokemon, list)).orElse(null);

    }

    /**
     * this method gets pokemon id
     *
     * @param namePokemon pokemon name
     * @return object with response according action
     */
    private GetPokemonResponse getID(String namePokemon) {

        return pokemonTransformer.maToPokemonIdResponse(pokemonApiDao.getPokemon(namePokemon));
    }

    /**
     * this method gets pokemon name
     *
     * @param namePokemon pokemon name
     * @return object with response according action
     */
    private GetPokemonResponse getName(String namePokemon) {

        return pokemonTransformer.maToPokemonNameResponse(pokemonApiDao.getPokemon(namePokemon));
    }

    /**
     * this method gets pokemon locations
     *
     * @param namePokemon pokemon name
     * @return object with response according action
     */
    private GetPokemonResponse getLocationAreaEncounters(String namePokemon) {

        return Optional.of(pokemonApiDao.getPokemon(namePokemon))
                .map(responsePokemon -> pokemonTransformer.maToPokemonLocalAreaResponse(pokemonApiDao
                        .getLocalAreaEncounter(responsePokemon.getLocationAreaEncounters())))
                .orElse(null);
    }

    /**
     * this method save in database
     *
     * @param pokemonRequest     object request
     * @param httpServletRequest object with client ip
     */
    private void saveInfoRequest(GetPokemonRequest pokemonRequest, HttpServletRequest httpServletRequest) {

        pokemonRepositoryDao.save(infoRequestTransformer.mapToGetInformationRequest(pokemonRequest, httpServletRequest));

    }


    private GetPokemonResponse evaluateAction(GetPokemonRequest request) {

        switch (request.getAction().toLowerCase()) {
            case ABILITIES:
                return getAbilities(request.getName().toLowerCase());
            case EXPERIENCIE:
                return getBaseExperience(request.getName().toLowerCase());
            case ITEMS:
                return getHeldItems(request.getName().toLowerCase());
            case ID:
                return getID(request.getName().toLowerCase());
            case NAME:
                return getName(request.getName().toLowerCase());
            case LOCATION:
                return getLocationAreaEncounters(request.getName().toLowerCase());
            default:
                return null;

        }

    }

}
