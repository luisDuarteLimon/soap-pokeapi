package com.challenge.soap.pokemon.service;


import com.challenge.soap_pokemon.GetPokemonRequest;
import com.challenge.soap_pokemon.GetPokemonResponse;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public interface IPokemonService {

    GetPokemonResponse getPokemonService(GetPokemonRequest namePokemon, HttpServletRequest httpServletRequest);


}
